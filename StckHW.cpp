﻿

#include <iostream>
using namespace std;

class Stck {
private:
    int* p;
    int n;
    int a;
    int pp;
    int currentindex = 0;
public:
    Stck() {
        cout << "Enter stack size: ";
        cin >> n;
        p = new int[n];
        currentindex = n-1;

        for (int i = 0; i <n; i++)
        {
            p[i] = 2 * i;
            cout << p[i];

        }
        //вызов функций pop а затем уже push
        pop();
        push();
        //очистка памяти
        delete p;

    }


    //функция котрая снимает последнее записанное значение
   //сохраняет его в переменную и выводит снятое значение отдельно
   //через вызов переменной, куда оно записано.
    void pop() {
        pp = p[currentindex];
        //проверка, что при декрементации индекс не станет меньше нуля  
        if (currentindex == 0)
        {
            cout << "Stack is empty!\n";
            return;
        }

        currentindex--;
        cout << "\n" << "After pop: ";
        for (int i = 0; i <= currentindex; i++)
        {
            cout << p[i];

        }
        cout << "\n" << "Requested value:" << pp;
        cout << "\n" << currentindex;
    }
    //функция, которая добавляет значения в следующую пустую ячейку массива
    void push() {
        // эта проверка не позволяет перезаполнить стек
        if (currentindex == n-1) {
            cout << "Stack is full!";
            return;
        }
        currentindex++;
        cout << "\n" << "enter new stack value: ";
        cin >> a;
        p[currentindex] = a;

        for (int i = 0; i <= currentindex; i++)
        {
            cout << p[i];
        }
    }
   

};

int main()
{

    //Вызов класса для вывода на консоль
    Stck s;
    return 0;
}